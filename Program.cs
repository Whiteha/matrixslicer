﻿using System;
using System.Collections;
using System.IO;
using System.Text;

public class Test
{
    private static void PrintMatrix(short[] data)
    {
        int size = (int)Math.Sqrt(data.Length);
        string s = "";
        for (int i = 0; i < size; ++i)
        {
            s = "";
            for (int j = 0; j < size; ++j)
                s += data[i * size + j] + "\t";
            Console.WriteLine(s);
        }

        Console.WriteLine("===================");
    }

    public static void Slice(short[] data, ushort sliceSize, int overlapping = 0)
    {
        int rowSizeInCells = (int)Math.Sqrt(data.Length);

        int subMatrixInLineCount = 0; //rowSizeInCells / (sliceSize - overlapping) - (overlapping + X);
        for (int i = 0; i + sliceSize <= rowSizeInCells; i += (sliceSize - overlapping))
            ++subMatrixInLineCount;

        int subMatrixTotalCount = subMatrixInLineCount * subMatrixInLineCount;

        short[] subMatrix = new short[sliceSize * sliceSize];
        for (int
			rowOffset = 0,
            rowBase = 0,
            colBase = 0,
            colOffset = 0,
            matrixInLineCounter = 0,
            subMatrixCellCounter = 0,
            subMatrixCounter = 0;
            subMatrixCounter < subMatrixTotalCount;)
		{
			subMatrix[subMatrixCellCounter] = data[(rowBase + rowOffset) * rowSizeInCells + colBase + colOffset];
			++subMatrixCellCounter;

			if (subMatrixCellCounter == subMatrix.Length)
			{
				subMatrixCellCounter = 0;
				++subMatrixCounter;

				PrintMatrix(subMatrix);				
			}

			if (colOffset == (sliceSize - 1))
			{
				colOffset = 0;
				if (rowOffset == (sliceSize - 1))
                {
					if ((subMatrixInLineCount - matrixInLineCounter) == 1)
					{
                        matrixInLineCounter = 0;
						rowBase += (sliceSize - overlapping);
					}
					else
					{
						++matrixInLineCounter;
					}

					rowOffset = 0;
					colBase = (matrixInLineCounter) * (sliceSize - overlapping);
				}
				else
				{
					++rowOffset;
				}
			}
			else
			{
				++colOffset;
			}
		}
	}

    private static short[] GetMatrixWithSize(int size)
    {
        short[] data = new short[size * size];

        for (int i = 0; i < data.Length; ++i)
        {
            data[i] = (short)i;
        }

        return data;
    }

    private static void Case1()
    {
        short[] data = GetMatrixWithSize(8);
        PrintMatrix(data);

        //Slice(data, 2, 0);
        //Slice(data, 2, 1);

        //Slice(data, 3, 0);
        //Slice(data, 3, 1);
        //Slice(data, 3, 2);

        Slice(data, 4, 0);
        Slice(data, 4, 1);
        Slice(data, 4, 2);
        Slice(data, 4, 3);

        //Slice(data, 5, 0);
        //Slice(data, 5, 1);
    }

    public static void Main()
    {
        Case1();
    }
}